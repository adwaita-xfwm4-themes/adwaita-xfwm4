# Adwaita-xfwm4

I've made an XFWM4 theme that looks consistent with GNOME's Adwaita (v3.30). Feel free to suggest any improvements and tweak it to your liking!

# Screenshot

![Screenshot](screenshot.png)

# Installation

Download the zip file and extract it to `~/.themes` directory.

